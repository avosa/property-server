require 'rails_helper'

RSpec.describe PropertyParamsValidator do
  subject { described_class.new(models_validators_property_params).validate }

  let(:params) do
    {lat: 52.5342963, lng: 13.4236807, property_type: 'apartment', marketing_type: 'sell'}
  end

  context 'if all params are valid' do
    let(:models_validators_property_params) { params }

    it 'should not return errors' do
      expect(subject.errors.full_messages).to be_empty
    end
  end

  context 'if params are empty' do
    let(:models_validators_property_params) { params.except(:marketing_type) }

    it 'should return an error' do
      expect(subject.errors.full_messages).to include('Marketing type cannot be empty, input a valid value')
    end
  end

  context 'if params are invalid' do
    let(:models_validators_property_params) { params.merge(property_type: 'penthouse') }

    it 'should return an error' do
      expect(subject.errors.full_messages).to include('Property type is invalid')
    end
  end
end