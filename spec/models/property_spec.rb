require 'rails_helper'

RSpec.describe Property, type: :model do
    describe '#search_locale' do
        it 'returns the search locale' do
            expect(Property::SEARCH_LOCALE).to eq(5000)
        end
    end
end