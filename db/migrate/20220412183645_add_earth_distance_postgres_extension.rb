class AddEarthDistancePostgresExtension < ActiveRecord::Migration[6.1]
  def change
    enable_extension 'cube'
    enable_extension 'earthdistance'
    enable_extension "plpgsql"
  end
end

=begin
Here's what the migration above is doing:
1. The enable_extension method is used to enable the cube and earthdistance extensions.
2. The enable_extension method is also used to enable the plpgsql extension.
=end