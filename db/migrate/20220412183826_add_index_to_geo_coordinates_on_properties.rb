class AddIndexToGeoCoordinatesOnProperties < ActiveRecord::Migration[6.1]
  def up
    execute <<-SQL.squish
      CREATE INDEX availables_properties ON properties
      USING gist (earth_box(ll_to_earth(lat, lng), 5000));
    SQL
  end

  def down
    execute "DROP INDEX availables_properties;"
  end
end

=begin
Here's what the migration above is doing:
1. It creates a new index on the properties table, called availables_properties.
2. It uses the gist index type, which is a special type of index for spatial data.
3. It uses the earth_box function to create a bounding box around the property's location.
4. It uses the ll_to_earth function to convert the latitude and longitude to a point on the Earth's surface.
5. It uses the 5000 parameter to specify the size of the bounding box in meters.
=end

=begin
We have a long string and we don't want to write it on a single line, and hence we use a HereDoc for it. 
But still, we don't want to keep all the newline characters and white spaces that the HereDoc preserves. 
So, we use the squish method i.e SQL.squish to remove all the newline characters and white spaces.
=end