class Api::V1::PropertiesController < ApplicationController
    include Paginable
    before_action :validate_params
  
    def index
      properties = Api::V1::Properties::SearchNearestProperties.new(property_params).initiate_search
      # to_be_paginated = properties
      paginated = paginate(properties)
  
      properties.present? ? render_collection(paginated) :  :not_found
    end
  
    private
  
    def validate_params
      validator = PropertyParamsValidator.new(property_params).validate
      return if validator.errors.empty?
  
      render json: {errors: validator.errors.full_messages}, status: :unprocessable_entity
    end
  
    def serializer
      PropertySerializer
    end
  
    def property_params
      params.permit(
        :property_type,
        :marketing_type,
        :lat,
        :lng
      )
    end
end