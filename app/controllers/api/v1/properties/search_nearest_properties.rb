class Api::V1::Properties::SearchNearestProperties
    attr_reader :lat, :lng, :property_type, :marketing_type, :radius
  
    def initialize(params={})
      @lat = params[:lat]
      @lng = params[:lng]
      @property_type = params[:property_type]
      @marketing_type = params[:marketing_type]
      @radius = Property::SEARCH_LOCALE
    end
  
    def initiate_search
      Property.where(property_type: property_type, offer_type: marketing_type)
        .where("earth_box(ll_to_earth(properties.lat, properties.lng), #{radius}) @> ll_to_earth(#{lat}, #{lng})")
        .where("earth_distance(ll_to_earth(properties.lat, properties.lng), ll_to_earth(#{lat}, #{lng})) < #{radius}")
    end
end
  
  
=begin
This class in a nutshell:
  1. It's finding all the properties that match the property_type and marketing_type
  2. It's finding all the properties that are within the radius of the lat and lng
=end