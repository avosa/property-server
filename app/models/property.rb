class Property < ApplicationRecord
    SEARCH_LOCALE = 5000.freeze # distance in meters i.e. 1km = 1000m
end
    
=begin
Here's what the above class is doing
    1. Returns an array of Property objects withing 5km distance which are hashed
    2. freeze prevents modification to the Hash (returns the frozen object)
=end