class PropertyParamsValidator
    include ActiveModel::Validations
  
    attr_reader :marketing_type, :property_type, :lng , :lat
  
    LATITUDE_PATTERN_REGEXP = /^-?([1-8]?\d(?:\.\d{1,})?|90(?:\.0{1,})?)$/.freeze
    LONGITUDE_PATTERN_REGEXP = /^-?((?:1[0-7]|[1-9])?\d(?:\.\d{1,})?|180(?:\.0{1,})?)$/.freeze
    VALID_MARKETING_TYPES = %w(rent sell).freeze
    VALID_PROPERTY_TYPES = %w(apartment single_family_house).freeze
  
    def initialize(params={})
      @lat = params[:lat]
      @lng = params[:lng]
      @property_type = params[:property_type]
      @marketing_type = params[:marketing_type]
    end
  
    def validate
      validate_coordinates_presence
      validate_property_data_presence
      validate_coordinates_matching
      validate_property_type_matching if property_type.present?
      validate_marketing_type_matching if marketing_type.present?
  
      self
    end
  
    def validate_coordinates_presence
      errors.add(:latitude, 'cannot be empty, input a valid value') unless lat.present?
      errors.add(:longitude, 'cannot be empty, input a valid value') unless lng.present?
    end
  
    def validate_property_data_presence
      errors.add(:property_type, 'cannot be empty, input a valid value') unless property_type.present?
      errors.add(:marketing_type, 'cannot be empty, input a valid value') unless marketing_type.present?
    end
  
    def validate_coordinates_matching
      errors.add(:latitude, 'is invalid') if lat.present? && !lat.to_s.match(LATITUDE_PATTERN_REGEXP)
      errors.add(:longitude, 'is invalid') if lng.present? && !lng.to_s.match(LONGITUDE_PATTERN_REGEXP)
    end
  
    def validate_property_type_matching
      return if VALID_PROPERTY_TYPES.include?(property_type)
  
      errors.add(:property_type, 'is invalid')
    end
  
    def validate_marketing_type_matching
      return if VALID_MARKETING_TYPES.include?(marketing_type)
  
      errors.add(:marketing_type, 'is invalid')
    end
end
    
=begin
Here's what the above class is doing in a nutshell:
    1. It's a class that includes ActiveModel::Validations. This gives us access to the ActiveModel::Validations::ClassMethods#validates method.
    2. It has a validate method that calls all the other validation methods.
    3. It has a validate_coordinates_presence method that checks if the lat and lng params are present.
    4. It has a validate_property_data_presence method that checks if the property_type and marketing_type params are present.
    5. It has a validate_coordinates_matching method that checks if the lat and lng params match the regexp patterns.
    6. It has a validate_property_type_matching method that checks if the property_type param is included in the VALID_PROPERTY_TYPES array.
    7. It has a validate_marketing_type_matching method that checks if the marketing_type param is included in the VALID_MARKETING_TYPES array.
=end