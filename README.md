# Property server

[property-server](https://gitlab.com/avosa/property-server.git) is a Restful API with one endpoint, that returns properties list within 5 km from given coordinates. 

# Getting started 

To start testing this microservice, you need to have [Ruby 3.0.0](https://www.ruby-lang.org/en/downloads/) and [Rails 6.1.5](https://rubygems.org/gems/rails/versions/6.1.4#:~:text=4,by%20favoring%20convention%20over%20configuration.) installed on your Mac/PC. 

For testing purposes, you need either [Postman](https://www.postman.com/), Visual Studio Code Extension like [Thunder Client](https://www.thunderclient.com/) or a similar extension installed as well. You may also opt to use [Curl](https://curl.se/) if you are a CLI geek! 😎

# Steps
```sh
git clone https://gitlab.com/avosa/property-server.git
```
```sh
cd ~/property-server
```

```sh
bundle install
```
```sh
rails db:create
```
```sh 
psql property_server_development < properties.sql
```
```sh
rails db:migrate
```

```sh
rails s
```

### Run tests

```sh
bundle exec rspec -fd
```

# Endpoints

# GET `api/v1/properties`

Sample json inputs 

```json
{
  "property_type": "apartment",
  "marketing_type": "sell",
  "lat": 52.5342963,
  "lng": 13.4236807
}
```

## Expected json output 

### 1. Non empty response 

```json
{
  "data": [
    {
      "id": "5506451",
      "type": "property",
      "attributes": {
        "house_number": null,
        "street": null,
        "city": "Berlin",
        "zip_code": "12627",
        "lat": "52.5303883",
        "lng": "13.6236423",
        "price": "34000.0"
      }
    },
    ,,,,,,
    
    ,,,,,,
    {
      "id": "11098285",
      "type": "property",
      "attributes": {
        "house_number": null,
        "street": null,
        "city": "Berlin",
        "zip_code": "12679",
        "lat": "52.5426585",
        "lng": "13.5493627",
        "price": "360050.0"
      }
    }
  ],
  "meta": {
    "total": 6,
    "pages": 1
  }
}
```

### 2. Empty response 

To reproduce this, enter a zero i.e. numerical zero (0), data under either ```lat```, ```lng``` or both into the required JSON input.

For example:

```json
{
  "lat": 0,
  "lng": 0,
  "property_type": "apartment",
  "marketing_type": "sell"
}
```

The above example will return 404 error.


### 3. Error response 

To reproduce this, ignore inputting any value to the required JSON input.

The expected output should look like below:

```json
{
  "errors": [
    "Latitude cannot be empty, input a valid value",
    "Longitude cannot be empty, input a valid value",
    "Property type cannot be empty, input a valid value",
    "Marketing type cannot be empty, input a valid value"
  ]
}
```

Enjoy!

# Author
[Webster Avosa](https://github.com/avosa)
